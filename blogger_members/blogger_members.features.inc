<?php

/**
 * Implementation of hook_imagecache_default_presets().
 */
function blogger_members_imagecache_default_presets() {
  $items = array(
    'blogger_adminer' => array(
      'presetname' => 'blogger_adminer',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale_and_crop',
          'data' => array(
            'width' => '70',
            'height' => '70',
          ),
        ),
      ),
    ),
    'blogger_avatar' => array(
      'presetname' => 'blogger_avatar',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale_and_crop',
          'data' => array(
            'width' => '75',
            'height' => '75',
          ),
        ),
      ),
    ),
    'blogger_members' => array(
      'presetname' => 'blogger_members',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale_and_crop',
          'data' => array(
            'width' => '40',
            'height' => '50',
          ),
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function blogger_members_views_api() {
  return array(
    'api' => '2',
  );
}
