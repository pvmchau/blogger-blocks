<?php

/**
 * Implementation of hook_default_fe_block_settings().
 */
function blogger_members_default_fe_block_settings() {
  $export = array();

  // academicroom
  $theme = array();

  $theme['views-member_blogger-block_1'] = array(
    'module' => 'views',
    'delta' => 'member_blogger-block_1',
    'theme' => 'academicroom',
    'status' => '1',
    'weight' => '-172',
    'region' => 'right',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '1',
    'pages' => 'blogs
blogs/*',
    'title' => '',
    'cache' => '-1',
  );

  $theme['views-member_blogger-block_2'] = array(
    'module' => 'views',
    'delta' => 'member_blogger-block_2',
    'theme' => 'academicroom',
    'status' => '1',
    'weight' => '-171',
    'region' => 'right',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '1',
    'pages' => 'blogs
blogs/*',
    'title' => '',
    'cache' => '-1',
  );

  $theme['views-members-block_1'] = array(
    'module' => 'views',
    'delta' => 'members-block_1',
    'theme' => 'academicroom',
    'status' => 0,
    'weight' => '-168',
    'region' => '',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '0',
    'pages' => '',
    'title' => '',
    'cache' => '-1',
  );

  $export['academicroom'] = $theme;

  $theme_default = variable_get('theme_default', 'garland');
  $themes = list_themes();
  foreach ($export as $theme_key => $settings) {
    if ($theme_key != $theme_default && empty($themes[$theme_key]->status)) {
      unset($export[$theme_key]);
    }
  }
  return $export;
}
